from flask import Flask, render_template, url_for, abort, redirect
from address import Address
from note import Note

app = Flask(__name__)

addresses = [Address("Ameqran Corine", "379 Garfield Ave.", "Montreal", "Quebec"), Address("Sixto Karine", "16 SW. Thomas St.", "Montreal", "Quebec"), Address("Louiza Apurva", "353 Piper Street", "Ottawa", "Ontario")]
notes = [Note(1, "This is note 1"), Note(2, "This is note 2"), Note(3, "This is note 3")]

# @app.errorhandler(404)
# def address_not_found(e):
#     return render_template('templates/customer404.html', 404)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('custom404.html'), 404

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/addressbook/<string:name>")
def address_book_search(name):
    if name == "":
        return redirect(url_for('address_book'))
    for a in addresses:
        if a.name == name:
            return a.str()
    
    abort(404)
            
@app.route("/addressbook")
def address_book():
    list = "<ul>"
    for a in addresses:
        list += f"<li><a href='{url_for('address_book_search', name=a.name)}'>{a.str()}</a></li>"
        
    list += "</ul>"
    
    return list

@app.route("/notes/<int:id>")
def note_search(id):
    if id == "":
        return redirect(url_for('note_book'))
    for n in notes:
        if n.id == id:
            return n.str()
    
    return redirect(url_for('note_book'))            
            
@app.route("/notes")
def note_book():
    list = "<ul>"
    for n in notes:
        list += f"<li><a href='{url_for('note_search', id=n.id)}'>Note {n.id}</a></li>"
        
    list += "</ul>"
    
    return list